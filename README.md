back-it-up

Copyright 2020 by Jerry Penner
License: GPLv3

# Overview

*back-it-up* backs up file systems on multiple hosts to a single
target device.  It stores the backed-up files in their original form
within a host-file system directory structure so that file restoration
is as simple as mounting the backup volume and copying out the files
and directories of interest.


# License

back-it-up is licensed under the GNU Public License version 3.

back-it-up is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


# Installation

To install into `/usr/local`, type:

        sudo make install

The `back-it-up` script and its manual page are installed.

See the manual page for full documentation on configuration and use.


This is a summary of the steps to perform backups:

 1. Create configuration file naming the machines and their file
    systems to be backed up.  See "Configuration Files" in the
    man-page.

 2. Configure passwordless ssh from the root account on the backup
    host to the root account on all other machines to be backed up.
    See PASSWORDLESS SSH KEYS in the man-page.

 3. Mount the backup device.

 4. Perform the backup:

        truecrypt /dev/sdd1 /media/backup
        sudo back-it-up CONFIG_FILE
        truecrypt -d /media/backup

 5. Unmount the backup device.
