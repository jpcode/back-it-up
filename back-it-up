#!/bin/bash
#
#  Backup one or more machines to the backup-device on which this
#  script is stored.
#
#  For usage info, execute:
#
#    back-it-up -h
#
#  See `man back-it-up` for documentation.
#
#  Copyright 2020-2021, Jerry Penner.  All rights reserved.
#  back-it-up is released under the GPLv3 license.  See COPYING for details.


# SSH key type to use for ${SUDO_USER} when doing remote rsync
## KEY_TYPE=rsa
USER_HOME="$(getent passwd ${SUDO_USER} |awk -F: '{print $6}')"
## KEY="${USER_HOME}/.ssh/id_${KEY_TYPE}"
## SSH_OPTS="-i ${KEY} -l ${SUDO_USER}"

# File system types to look for
FS_TYPES=ext3,ext4,vfat


fmt_date() {
    date "+%Y-%m-%d %H:%M:%S"
}


usage() {
    cat 1>&2 <<EOF
Usage: $(basename $0) [-d|--device-info MOUNT_POINT] | CONFIG_PATHNAME
   -c | --check-only               Check configuration file and exit.
   -d | --device-info MOUNT_POINT  Show device ID(s) and exit.
                                   You may provide multiple -d MOUNT_POINTs.
   -n | --dry-run                  Perform a dry-run; no files are actually
                                   backed up, but remote hosts are logged
                                   into and rsync --dry-run is executed.
   CONFIG_PATHNAME                 Run backup specified in config file.
EOF
}


error() {
    local CODE=$1
    shift
    if [ "$(type -t log)" == "function" ]; then
        log "$@"
        log "Backup failed"
    else
        echo "$(fmt_date): $@" 1>&2
    fi
    usage
    exit ${CODE}
}


log() {
    echo "$(fmt_date): $@" |tee -a ${LOG_FILE} 1>&2
}


#  Return the block device name on which the backup partition is
#  stored.
get_target_device_info() {  # TARGET_ROOT
    local BLOCK_DEV PARTITION SOURCE TARGET_ROOT="$1"

    SOURCE=$(df --output=source ${TARGET_ROOT} |tail -1)
    PARTITION=$(df --output=target ${TARGET_ROOT} |tail -1)
    [[ "${SOURCE}" =~ "truecrypt" ]] && \
        SOURCE=$(truecrypt --list "${PARTITION}" |awk '{print $2}')
    [[ "${SOURCE}" =~ "veracrypt" ]] && \
        SOURCE=$(veracrypt --text --list "${PARTITION}" |awk '{print $2}')
    BLOCK_DEV=$(lsblk --noheadings --list --output=PKNAME "${SOURCE}" \
		        |grep -v "$(basename "${SOURCE}")")
    set $(lsblk --noheadings --list --output VENDOR,MODEL,SERIAL,SIZE,NAME \
	        /dev/${BLOCK_DEV} |sed '/^ /d')
    echo $@
}


#  Rsync flags used:
#         --recursive       backup the entire subtree
#         --links           copy symbolic links as symbolic links
#         --hard-links      preserve hard links
#         --perms           preserve permissions
#         --executability   preserve exec bit
#         --owner           preserve owner
#         --group           preserve group
#         --times           preserve times
#         --devices         preserve device nodes
#         --specials        preserve block- and character-special nodes
#         --one-file-system do not cross file system boundaries
#         --delete-during   delete extranneous files from destination
#                           during the transfer
#         --exclude=.cache  Do not back up .cache directories.
#         --dry-run         Perform trial run without making changes.

backup_file_system() {  # HOST:PATH TARGET_BACKUP_PATH
    local SOURCE="$1" TARGET="$2"

    eval rsync \
         "${DRY_RUN}" \
         --recursive \
         --links --hard-links \
         --perms --executability --owner --group --times \
         --devices --specials \
         --one-file-system \
         --delete-during \
         --delete-excluded \
         $(for FILTER in "${FILTERS[@]}"; do
               echo -n "--filter \"${FILTER}\" "
           done) \
         "${SOURCE}" "${TARGET}"
}


#  Check host.
#   Return:
#     0: localhost
#     1: remote host unreachable
#     2; remote host responds
check_host() {
    [ "${HOST}" != "$(hostname)" ] && {
        ssh ${HOST} true >/dev/null 2>&1
        [ $? -ne 0 ] && {
            log "${HOST} is unreachable"
            return 1
        }
        ${CHECK_ONLY} && echo "${HOST} reachable"
        return 2
    }
    ${CHECK_ONLY} && echo "${HOST} reachable"
    return 0
}


perform_host() {
    :
}


check_backup() {
    [ -z "${HOST}" ] && error 7 "'backup' must be after a 'host' specifier"
    FS=""
    FILTERS=()
}


perform_backup() {
    local REMOTE_HOST="" RSYNC_FS TARGET TARGET_NAME

    check_host "${HOST}"
    case $? in
        1) return 1;;
        2) REMOTE_HOST="${HOST}:";;
    esac

    case ${FS} in
	/) TARGET_NAME="/root"
           RSYNC_FS="${FS}"
           ;;
        /*) TARGET_NAME="${FS}"
            RSYNC_FS="${FS}/"
            ;;
	*) TARGET_NAME="/${FS}"
           RSYNC_FS="${FS}/"
           ;;
    esac

    TARGET=${TARGET_ROOT}/${HOST}${TARGET_NAME}
    mkdir -p ${TARGET}
    log "Back up ${HOST}:${FS} ==> ${TARGET}${DRY_RUN_NOTE}"
    backup_file_system ${REMOTE_HOST}${RSYNC_FS} ${TARGET} 2>&1 \
        |grep -v 'file has vanished' |tee -a "${LOG_FILE}"

    #  Reset file system and filters once complete.
    FS=""
    FILTERS=()
}


#  Process the configuration file.
#
#  When parsing tags, the *backup* tag has a callback function named
#  ${HANDLER_PREFIX}_backup.  The prefix allows for a check phase and
#  an action phase (or whatever else you want).
#
#  The CONFIG_FILENAME is the name of the configuration to be used.

process_config() {  # HANDLER_PREFIX CONFIG_FILENAME
    HANDLER="$1"
    LINE_NUM=0
    eval {fd}<"$2"
    trap "exec {fd}<&-" RETURN

    while :; do
        read -u $fd LINE || break

        LINE_NUM=$((LINE_NUM + 1))
        case "${LINE/\#*}" in
            "") continue;;
            *)
                set ${LINE/\#*}
                TAG="$1"
                shift
                VALUE="$@"
                case ${TAG} in
                    log)
                        LOG_FILE="$(realpath "$(eval echo "${VALUE}")")"
                        ;;
                    target)
                        TARGET_ROOT="$(realpath "${VALUE}")"
                        ;;
                    target-device)
                        DEVICES+=("${VALUE}")
                        ;;
                    host)
                        check_config
                        HOST="$1"
                        eval ${HANDLER}_${TAG} ${VALUE}
                        ;;
                    fs)
                        check_config
                        FS="$1"
                        ;;
                    filter)
                        check_config
                        FILTERS+=("$*")
                        ;;
                    backup)
                        check_config
                        eval ${HANDLER}_${TAG} ${VALUE}
                        ;;
                    *) error 6 "bad tag ${TAG} on line ${LINE_NUM}"
                       return $?
                       ;;
                esac
                ;;
        esac
    done

    eval {fd}<&-
}


#  Check configuration settings.
#
#  1. Check TARGET_ROOT is set and is valid.
#  2. Check DEVICES contains a match to the device on which
#     TARGET_ROOT is mounted.

check_config() {
    [ -z "${TARGET_ROOT}" ] && error 2 "TARGET_ROOT is not specified."
    [ ! -d "${TARGET_ROOT}" -o ! -w "${TARGET_ROOT}" ] && \
        error 3 "'${TARGET_ROOT}' is not a writeable directory."
    local GOOD_DEVICE=false
    local ID
    local DEVICE_ID="$(get_target_device_info "${TARGET_ROOT}")"
    for ID in "${DEVICES[@]}"; do
        if [[ "${DEVICE_ID}" =~ "${ID}" ]]; then
            GOOD_DEVICE=true
            break
        fi
    done
    ${GOOD_DEVICE} || error 8 "Configured backup device not found ${DEVICES[@]}."
}


#  Back it up

DEVICES_ONLY=false
CHECK_ONLY=false
DRY_RUN=""
DRY_RUN_NOTE=""

while [ -n "$1" ]; do
    case "$1" in
        -d|--device-info)
            DEVICES+=("$(get_target_device_info "$2")")
            DEVICES_ONLY=true
            shift
            ;;
        -h|-\?|--help)
            usage
            exit 0
            ;;
        -c|--check-only)
            CHECK_ONLY=true
            ;;
        -n|--dry-run)
            DRY_RUN="--dry-run"
            DRY_RUN_NOTE=" (dry run)"
            ;;
        *)
            [ -n "${CONFIG_FILE}" ] && error 10 "CONFIG_FILE specified twice"
            CONFIG_FILE="$1"
            ;;
    esac
    shift
done

${DEVICES_ONLY} && {
    for DEVICE in "${DEVICES[@]}"; do
        echo "${DEVICE}"
    done
    exit 0
}

[ ! -f "${CONFIG_FILE}" ] && \
    error 1 "Cannot read configuration file: '${CONFIG_FILE}'."

process_config check "${CONFIG_FILE}" || \
    error 4 "configuration ${CONFIG_FILE} has errors"
check_config

${CHECK_ONLY} && {
    echo "Configuration okay: ${CONFIG_FILE}" 1>&2
    exit 0
}

[ -z "${LOG_FILE}" ] && error 5 "No 'log' file specified in configuration file."

log "Backup device: $(get_target_device_info "${TARGET_ROOT}")"
log "Target status before backup:"
log "$(df -h "${TARGET_ROOT}" |tail -1)"
process_config perform "${CONFIG_FILE}"
cp -p ${CONFIG_FILE} \
   ${TARGET_ROOT}/$(date "+%Y%m%d_%H%M%S")_"$(basename "${CONFIG_FILE}")"
log "Target status after backup:"
log "$(df -h "${TARGET_ROOT}" |tail -1)"
log "Backup completed."
