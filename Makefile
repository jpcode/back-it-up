#  Makefile for back-it-up

INSTALL_DIR = /usr/local


.PHONY: clean test


clean:
	@rm -f *~ *.log

test:
	./test_backup

install:
	cp -p back-it-up $(INSTALL_DIR)/bin/
	mkdir -p $(INSTALL_DIR)/man/man8
	gzip -c back-it-up.8 >$(INSTALL_DIR)/man/man8/back-it-up.8.gz

uninstall:
	rm -f $(INSTALL_DIR)/bin/back-it-up
	rm -f $(INSTALL_DIR)/man/man8/back-it-up.8.gz
