.TH BACK-IT-UP "8" "v1.0, March 2020" "Jerry Penner"
.SH NAME
back-it-up \- back up files from network-connected machines
.SH SYNOPSIS
.SY back-it-up
.OP "-d | --device-info"  MOUNT_POINT
.OP CONFIG_PATHNAME
.SY back-it-up
.OP -h
.YS
.PP
.SH DESCRIPTION
.PP
\fBback-it-up\fP backs up file systems, or portions thereof, from
multiple computers, to a single target volume.  It stores the
backed-up files in their original form within a host-file system
directory structure so that file restoration is as simple as mounting
the backup volume and copying out the files and directories of
interest.  The machines and file systems to be backed up are specified
in the configuration file, \fICONFIG_PATHNAME\fP.

Typically, \fBback-it-up\fP is used to back up entire file systems,
and thus requires root privileges.  The \fBsudo\fP command must be
used to run \fBback-it-up\fP in this situation; it will not run if you
login as the root user.  Furthermore, you must have sudo configured on
remote machines to run \fBrsync\fP without requiring a password.  See
the \fBCONFIGURING SUDO FOR RSYNC\fP section below for details.

If all files to be backed up are owned by the user, and the backup
volume is writeable by the user, \fBback-it-up\fP does not
need to be run under \fBsudo\fP.
.TP
\fB-h\fP | \fB--help\fP
Print a usage help message and exit.
.TP
\fB-c\fP | \fB\-\-check\-only\fP
Checks the configuration and exits.  Checks all SSH connections to
hosts specified in the configuration file.
.TP
\fB-d\fP | \fB\-\-device\-info\fP \fBMOUNT_POINT\fP
Show device ID(s) and exit. You may provide multiple \fB\-d
MOUNT_POINT\fP options.  This option provides a way to determine the
correct \f
.TP
\fB-n\fP | \fB\-\-dry\-run\fP
Perform a dry-run; no files are actually backed up, but remote hosts
are logged into and rsync --dry-run is executed.
.TP
\fBCONFIG_PATHNAME\fP
Run backup specified in config file.  See \fBCONFIGURATION FILES\fP below.

.SH USING BACK-IT-UP

Before running \fBback-it-up\fP, three configuration steps must be
completed:
.IP 1.
Create a configuration file describing the target volume and device,
log-file, and hosts and file systems to be backed up.
.IP 2.
Configure passwordless SSH to all network machines in the
configuration file.  If the only host to be backed up is the host on
which the script is run, no SSH configuration is necessary.
.IP 3.
Configure sudo to run \fBrsync\fP as root on all network machines
listed in the configuration file.
.PP
Performing the back up is typically a three-step operation:
.IP 1.
Mount the backup volume.
.IP 2.
Run \fBback\-it\-up\fP \fICONFIG_FILE\fP
.IP 3.
Unmount the backup volume.

.SH CONFIGURATION FILES

Configuration files are line-oriented and contain a tag and possibly
arguments.  Comments may be included by prefixing them with the #
character.  Whitespace is ignored, except in target-device IDs and
filter rules.  It is recommended to indent the tags as shown in the
example below, to make clear the relationships between hosts, file
systems, filters, and backups.

Other than the \fBfilter\fP tag, all tags are required, and should be
specified in the order given here.

.B log \fIPATHNAME\fP
.RS
Required.

Specifies pathname of the log file to which this backup session writes
its log messages.
.RE

.B target \fIPATHNAME\fP
.RS
Required.

Specifies PATHNAME to backup target root directory.
.RE
.B target-device \fIDEVICE_ID\fP
.RS
Required.  Multiple \fBtarget-device\fP lines may be specified and
one of those devices must be online for the backup to be performed.

This is the device identifier for the backup volume.  This specifier
is a safety check to ensure that backups are not run when the backup
volume is not present.
.RE

.B host \fIHOSTNAME\fP
.RS
Required.

Names a host containing file systems to be backed up.  Passwordless
SSH must be configured from the backup host machine to each machine
specified by the \fBhost\fP tag.
.RE

.B fs \fIFILE_SYSTEM_ROOT\fP
.RS
Required.

Specify the root-directory of the file system to be backed up.
.RE

.B filter \fIRSYNC_FILTER\fP
.RS
Optional.

See the rsync documentation for FILTER syntax.  Typically, exclude
filters are specified so the backup ignores files or directories.  An
example of an exclude filter line is:
.IP
.EX
filter - .cache/
.EE
.PP
Multiple \fBfilter\fP lines may be specified and they are all utilized
by the next backup.
.RE

.B backup
.RS
Required.  Has no parameters.

\fBbackup\fP performs a backup using the most-recent \fBhost\fP,
\fBfs\fP, and optionally \fBfilter\fP tag lines.  If this line is not
specified, back-it-up silently ignores the most recently specified
file system.

After the backup is performed, the file system and filters are
cleared.  Thus, to perform multiple back ups on a host, multiple
groups of fs, filter, and backup must be specified.
.RE

.SH EXAMPLE CONFIGURATION FILE
.PP
Here is an example configuration file illustrating all the keywords.
.EX

log /root/my-backup.log
target /media/backup
target-device Seagate Expansion Desk NA5M4MJL 1.8T sdd

host petunia
 fs /home
  filter - .cache/
  backup
 fs /opt/video
  filter - kids/
  filter - movies/
  filter - workdir/
  backup

host marigold
 fs /
  backup
.EE

.SH PASSWORDLESS SSH KEYS

In order to backup remote host's file systems, \fBback-it-up\fP uses
rsync, which in turn uses ssh.

The root account on the backup host machine must be able to login,
using passwordless-ssh, to all the hosts listed in the configuration
file.  Note that the configured host that is the one running the
backup does not need SSH keys to be configured.

First, ensure that the public/private keys have been created and the
public keys have been copied to the remote hosts appropriately.  Use
this recipe:
.IP 1.
Create \fBroot's\fP keys on backup host machine

.EX
sudo ssh-keygen -t ed25519
.EE
.IP 2.
Get the public key just generated

.EX
sudo cat /root/.ssh/id_ed25519.pub
.EE

.IP 2.
Login to each remote host and add this public key to the
\fBauthorized_keys\fP file for root.

.EX
ssh \fIHOSTNAME\fP
sudo mkdir -p /root/.ssh
sudo chmod 700 /root/.ssh
echo '\fIPUBLIC KEY\fP' |sudo tee -a /root/.ssh/authorized_keys
sudo chmod 600 /root/.ssh/authorized_keys
logout
.EE
.PP

.SH CONFIGURING SUDO FOR RSYNC

On the backup hosts, the user running \fBback-it-up\fP needs to be
able to run \fBrsync\fP as root without entering a password.

Add a line like this to the bottom of your \fB/etc/sudoers\fP file.
This line must come after lines that set general access controls for
all users, otherwise the change is wiped out.

.EX
\fIUSER_ID\fP ALL=NOPASSWD: /usr/bin/rsync
.EE

.SH AUTHOR
Jerry Penner
.SH COPYRIGHT
Copyright \(co 2020 Jerry Penner

License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
.br
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
